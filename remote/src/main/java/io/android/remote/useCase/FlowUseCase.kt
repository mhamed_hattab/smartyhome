package io.android.remote.useCase

import io.android.remote.utils.Resource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart
import java.net.ConnectException

@ExperimentalCoroutinesApi
abstract class FlowUseCase<in P, R>(private val coroutineDispatcher: CoroutineDispatcher) {

    private val CONNECTION_CODE:Int = -1
    private val GENERAL_ERROR_CODE:Int = -2

    operator fun invoke(parameters: P): Flow<Resource<R>> = execute(parameters)
        .catch { error ->
            when(error){
                is ConnectException -> {
                    emit(Resource.error("$CONNECTION_CODE", null))
                }
                else -> {
                    emit(Resource.error("$GENERAL_ERROR_CODE", null))
                }
            }
        }
        .onStart { emit(Resource.loading(null)) }
        .flowOn(coroutineDispatcher)

    protected abstract fun execute(parameters: P): Flow<Resource<R>>
}