package io.android.remote.model

data class Room(
    val imageRes: Int?=null,
    val name:String?=null,
    val countDevices:Int?=null
)
