package io.android.remote.dataManager


import io.android.remote.local.PreferenceStorage
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppDataRepository @Inject
constructor(
    private val dataStore: PreferenceStorage,
) : DataRepository {
    override suspend fun setUserName(userName: String) {
        dataStore.setUserName(userName)
        dataStore.setUserConnected(true)
    }

    override suspend fun getUserName(): Flow<String?> = dataStore.userName

    override suspend fun isUserConnected(): Flow<Boolean> = dataStore.isUserConnected

    override suspend fun setUserConnected(isConnected: Boolean) =
        dataStore.setUserConnected(isConnected)


}
