package io.android.remote.dataManager

import kotlinx.coroutines.flow.Flow

interface DataRepository {
    suspend fun setUserName(userName: String)
    suspend fun getUserName(): Flow<String?>
    suspend fun isUserConnected(): Flow<Boolean>
    suspend fun setUserConnected(isConnected:Boolean)

}