package io.android.remote.providerCoroutine

import kotlinx.coroutines.CoroutineDispatcher



interface CoroutineProvider {

    fun computation(): CoroutineDispatcher

    fun io(): CoroutineDispatcher

    fun ui(): CoroutineDispatcher
}
