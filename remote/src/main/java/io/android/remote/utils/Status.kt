package io.android.remote.utils


enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}