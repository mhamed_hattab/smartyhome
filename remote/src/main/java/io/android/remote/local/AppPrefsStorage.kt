package io.android.remote.local

import android.content.Context
import androidx.datastore.DataStore
import androidx.datastore.preferences.*
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class AppPrefsStorage @Inject constructor(
    @ApplicationContext context: Context
) : PreferenceStorage {

    private val dataStore: DataStore<Preferences> =
        context.createDataStore(name = "pref_think_it")

    private object PreferencesKeys {
        val PREF_KEY_USER_NAME = preferencesKey<String>("pref_key_user_name")
        val IS_USER_CONNECTED = preferencesKey<Boolean>("pref_is_user_connected")
    }

    override val isUserConnected: Flow<Boolean>
        get() = dataStore.data.catch { exception ->
            when (exception) {
                is IOException -> emit(emptyPreferences())
                else -> throw exception
            }
        }.map { preferences ->
            preferences[PreferencesKeys.IS_USER_CONNECTED] ?: false
        }

    override suspend fun setUserConnected(isConnected: Boolean) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.IS_USER_CONNECTED] = isConnected
        }
    }

    override val userName: Flow<String?>
        get() = dataStore.data.catch { exception ->
            when (exception) {
                is IOException -> emit(emptyPreferences())
                else -> throw exception
            }
        }.map { preferences ->
            preferences[PreferencesKeys.PREF_KEY_USER_NAME]
        }

    override suspend fun setUserName(userName: String) {
        dataStore.edit { preferences ->
            preferences[PreferencesKeys.PREF_KEY_USER_NAME] = userName
        }
    }

    override suspend fun clearPreferenceStorage() {
        dataStore.edit { preferences ->
            preferences.clear()
        }
    }
}