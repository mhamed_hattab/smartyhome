package io.android.remote.local

import kotlinx.coroutines.flow.Flow

interface PreferenceStorage {
    val isUserConnected: Flow<Boolean>
    suspend fun setUserConnected(isConnected: Boolean)
    val userName: Flow<String?>
    suspend fun setUserName(userName: String)
    suspend fun clearPreferenceStorage()
}