/**
 * Automatically generated file. DO NOT MODIFY
 */
package io.android.remote;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String LIBRARY_PACKAGE_NAME = "io.android.remote";
  public static final String BUILD_TYPE = "debug";
  // Field from build type: debug
  public static final boolean LOG = true;
}
