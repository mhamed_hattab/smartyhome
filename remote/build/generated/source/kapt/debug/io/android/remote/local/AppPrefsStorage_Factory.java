package io.android.remote.local;

import android.content.Context;
import dagger.internal.Factory;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppPrefsStorage_Factory implements Factory<AppPrefsStorage> {
  private final Provider<Context> contextProvider;

  public AppPrefsStorage_Factory(Provider<Context> contextProvider) {
    this.contextProvider = contextProvider;
  }

  @Override
  public AppPrefsStorage get() {
    return newInstance(contextProvider.get());
  }

  public static AppPrefsStorage_Factory create(Provider<Context> contextProvider) {
    return new AppPrefsStorage_Factory(contextProvider);
  }

  public static AppPrefsStorage newInstance(Context context) {
    return new AppPrefsStorage(context);
  }
}
