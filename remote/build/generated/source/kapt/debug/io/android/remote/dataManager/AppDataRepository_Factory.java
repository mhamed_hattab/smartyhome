package io.android.remote.dataManager;

import dagger.internal.Factory;
import io.android.remote.local.PreferenceStorage;
import javax.annotation.Generated;
import javax.inject.Provider;

@Generated(
    value = "dagger.internal.codegen.ComponentProcessor",
    comments = "https://dagger.dev"
)
@SuppressWarnings({
    "unchecked",
    "rawtypes"
})
public final class AppDataRepository_Factory implements Factory<AppDataRepository> {
  private final Provider<PreferenceStorage> dataStoreProvider;

  public AppDataRepository_Factory(Provider<PreferenceStorage> dataStoreProvider) {
    this.dataStoreProvider = dataStoreProvider;
  }

  @Override
  public AppDataRepository get() {
    return newInstance(dataStoreProvider.get());
  }

  public static AppDataRepository_Factory create(Provider<PreferenceStorage> dataStoreProvider) {
    return new AppDataRepository_Factory(dataStoreProvider);
  }

  public static AppDataRepository newInstance(PreferenceStorage dataStore) {
    return new AppDataRepository(dataStore);
  }
}
