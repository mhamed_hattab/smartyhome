Smarty Home
======================

This project is the Android app for Think-it Mobile code challenge.

# Development Environment

The app is written entirely in Kotlin and uses the Gradle build system.

# Architecture

The architecture is built around
[Android Architecture Components](https://developer.android.com/topic/libraries/architecture/).

We followed the recommendations laid out in the
[Guide to App Architecture](https://developer.android.com/jetpack/docs/guide)
when deciding on the architecture for the app. We kept logic away from
Activities and Fragments and moved it to
[ViewModel](https://developer.android.com/topic/libraries/architecture/viewmodel)s.
We observed data using
[LiveData](https://developer.android.com/topic/libraries/architecture/livedata)
and used the [Data Binding Library](https://developer.android.com/topic/libraries/data-binding/)
to bind UI components in layouts to the app's data sources.

We used a Repository layer for handling data operations. IOSched's data comes
from a few different sources -  user data is stored in

We implemented a lightweight domain layer, which sits between the data layer
and the presentation layer, and handles discrete pieces of business logic off
the UI thread.

We used [Navigation component](https://developer.android.com/guide/navigation)
to simplify into a single Activity app.

We used [DataStore](https://developer.android.com/topic/libraries/architecture/datastore) to store simple key,value into local file.

We used [Espresso](https://developer.android.com/training/testing/espresso/) for basic instrumentation tests and JUnit and
[Mockito](https://github.com/mockito/mockito) for unit testing.
