package com.thinkit.think_it_code_challenge.ui.main

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.asLiveData
import com.nhaarman.mockitokotlin2.mock
import com.thinkit.think_it_code_challenge.*
import com.thinkit.think_it_code_challenge.domain.SetUserNameUseCase
import io.android.remote.dataManager.AppDataRepository
import io.android.remote.dataManager.DataRepository
import io.android.remote.utils.Status
import junit.framework.Assert.assertEquals
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flowOf
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import timber.log.Timber

@ExperimentalCoroutinesApi
class MainViewModelTest {

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    @get:Rule
    var coroutineRule = MainCoroutineRule()

    private var testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    var mMockDataManager: DataRepository = mock()


    @Test
    fun on_click_continue() = coroutineRule.runBlockingTest {
        testCoroutineDispatcher = TestCoroutineDispatcher()
        val coroutineDispatcher = TestSchedulerProvider(testCoroutineDispatcher)
        val setUserNameUseCase =
            SetUserNameUseCase(mMockDataManager, coroutineDispatcher)
        val viewModel = MainViewModel(setUserNameUseCase)

        viewModel.onClickContinue()
        val navigateEvent = LiveDataTestUtil.getValue(viewModel.interactions)
        Assert.assertEquals(
            MainViewModel.Interactions.CLICK_CONTINUE.name,
            navigateEvent?.peekContent()?.name
        )

    }

    @Test
    fun submit_name_user() = coroutineRule.runBlockingTest {
        testCoroutineDispatcher = TestCoroutineDispatcher()
        val coroutineDispatcher = TestSchedulerProvider(testCoroutineDispatcher)
        val setUserNameUseCase =
            SetUserNameUseCase(mMockDataManager, coroutineDispatcher)
        val viewModel = MainViewModel(setUserNameUseCase)
        viewModel.submitName("UserName")
        viewModel.interactions.observeForTesting {
            Assert.assertEquals(
                MainViewModel.Interactions.SUBMIT_NAME.name,
                LiveDataTestUtil.getValue(viewModel.interactions)?.peekContent()?.name
            )
        }

    }



}