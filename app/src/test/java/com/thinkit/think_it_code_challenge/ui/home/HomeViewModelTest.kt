package com.thinkit.think_it_code_challenge.ui.home

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.nhaarman.mockitokotlin2.mock
import com.thinkit.think_it_code_challenge.*
import com.thinkit.think_it_code_challenge.domain.GetRoomsUseCase
import com.thinkit.think_it_code_challenge.domain.GetUserNameUseCase
import com.thinkit.think_it_code_challenge.domain.SetUserNameUseCase
import com.thinkit.think_it_code_challenge.ui.main.MainViewModel
import io.android.remote.dataManager.DataRepository
import io.android.remote.utils.Status
import junit.framework.TestCase
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher
import org.junit.Assert
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock

@ExperimentalCoroutinesApi
class HomeViewModelTest {
    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()
    @get:Rule
    var coroutineRule = MainCoroutineRule()

    private var testCoroutineDispatcher = TestCoroutineDispatcher()

    @Mock
    var mMockDataManager: DataRepository = mock()

    @Test
    fun get_user_name() = coroutineRule.runBlockingTest {
        testCoroutineDispatcher = TestCoroutineDispatcher()
        val coroutineDispatcher = TestSchedulerProvider(testCoroutineDispatcher)
        val getUserNameUseCase =
            GetUserNameUseCase(mMockDataManager, coroutineDispatcher)
        val getRoomsNameUseCase =
            GetRoomsUseCase(mMockDataManager, coroutineDispatcher)
        val viewModel = HomeViewModel(getUserNameUseCase,getRoomsNameUseCase)
        viewModel.getUserName()
        val navigateEvent = LiveDataTestUtil.getValue(viewModel.currentDate)
        Assert.assertNotNull(
            navigateEvent
        )

    }


    @Test
    fun get_data_rooms_has_observe() = coroutineRule.runBlockingTest {
        testCoroutineDispatcher = TestCoroutineDispatcher()
        val coroutineDispatcher = TestSchedulerProvider(testCoroutineDispatcher)
        val getUserNameUseCase =
            GetUserNameUseCase(mMockDataManager, coroutineDispatcher)
        val getRoomsNameUseCase =
            GetRoomsUseCase(mMockDataManager, coroutineDispatcher)
        val viewModel = HomeViewModel(getUserNameUseCase,getRoomsNameUseCase)
        viewModel.getDataRooms()
        val navigateEvent = LiveDataTestUtil.getValue(viewModel.rooms)
        Assert.assertNotNull(
            navigateEvent
        )

    }

    @Test
    fun get_data_rooms_success() = coroutineRule.runBlockingTest {
        testCoroutineDispatcher = TestCoroutineDispatcher()
        val coroutineDispatcher = TestSchedulerProvider(testCoroutineDispatcher)
        val getUserNameUseCase =
            GetUserNameUseCase(mMockDataManager, coroutineDispatcher)
        val getRoomsNameUseCase =
            GetRoomsUseCase(mMockDataManager, coroutineDispatcher)
        val viewModel = HomeViewModel(getUserNameUseCase,getRoomsNameUseCase)
        viewModel.getDataRooms()
        viewModel.rooms.observeForTesting {
            Assert.assertEquals(
                Status.SUCCESS,
                LiveDataTestUtil.getValue(viewModel.rooms)?.peekContent()?.status
            )
        }

    }
}