package com.thinkit.think_it_code_challenge

import io.android.remote.providerCoroutine.CoroutineProvider
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.TestCoroutineDispatcher


class TestSchedulerProvider @ExperimentalCoroutinesApi constructor(private val testCoroutineDispatcher: TestCoroutineDispatcher) : CoroutineProvider {

    @ExperimentalCoroutinesApi
    override fun computation(): CoroutineDispatcher {
        return testCoroutineDispatcher
    }

    @ExperimentalCoroutinesApi
    override fun io(): CoroutineDispatcher {
        return testCoroutineDispatcher
    }

    @ExperimentalCoroutinesApi
    override fun ui(): CoroutineDispatcher {
        return testCoroutineDispatcher
    }
}