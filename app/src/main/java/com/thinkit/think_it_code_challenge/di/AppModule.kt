package com.thinkit.think_it_code_challenge.di

import android.app.Application
import android.content.Context
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ApplicationComponent
import io.android.remote.dataManager.AppDataRepository
import io.android.remote.dataManager.DataRepository
import io.android.remote.providerCoroutine.AppCoroutineProvider
import io.android.remote.providerCoroutine.CoroutineProvider
import javax.inject.Singleton

@Module
@InstallIn(ApplicationComponent::class)
class AppModule {
    @Provides
    @Singleton
    internal fun provideContext(application: Application): Context {
        return application
    }

    @Provides
    @Singleton
    internal fun provideDataManager(appDataRepository: AppDataRepository): DataRepository {
        return appDataRepository
    }

    @Provides
    @Singleton
    internal fun provideGson(): Gson {
        return GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()
    }

    @Provides
    internal fun provideCoroutineProvider(): CoroutineProvider {
        return AppCoroutineProvider()
    }

}