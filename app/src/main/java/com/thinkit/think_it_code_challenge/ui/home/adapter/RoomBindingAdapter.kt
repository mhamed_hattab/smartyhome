package com.thinkit.think_it_code_challenge.ui.home.adapter

import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.databinding.BindingAdapter
import com.thinkit.think_it_code_challenge.R

@BindingAdapter("imgRes")
fun setImageDrawableRes(view: AppCompatImageView, imageRes: Int) {
    view.setImageResource(imageRes)
}

@BindingAdapter("devices")
fun setRoomDevicesCount(view: AppCompatTextView, count: Int) {
    view.text = if (count == 1) view.context.getString(
        R.string.room_devices,
        count,
        ""
    ) else view.context.getString(R.string.room_devices, count, "s")
}


@BindingAdapter("app:srcDrawable")
fun setImageDrawable(view: AppCompatImageView, resource: Int) {
    view.setImageResource(resource)
}