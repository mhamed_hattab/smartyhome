package com.thinkit.think_it_code_challenge.ui.main

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thinkit.think_it_code_challenge.domain.SetUserNameUseCase
import io.android.remote.result.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

@ExperimentalCoroutinesApi
class MainViewModel @ViewModelInject constructor(
    private val setUserNameUseCase: SetUserNameUseCase
) : ViewModel() {

    private val _keyboardVisibilityEvent = MutableLiveData<Boolean>(false)
    val keyboardVisibilityEvent: LiveData<Boolean> = _keyboardVisibilityEvent

    private val _isSubmitEnabled = MutableLiveData<Boolean>(false)
    val isSubmitEnabled: LiveData<Boolean> = _isSubmitEnabled

    private val _interactions = MutableLiveData<Event<Interactions>>()
    val interactions: LiveData<Event<Interactions>> = _interactions

    fun onClickContinue() {
        viewModelScope.launch {
            _interactions.postValue(Event(Interactions.CLICK_CONTINUE))
        }
    }

    fun submitName(name: String) {
        viewModelScope.launch {
            setUserNameUseCase.invoke(name).collect {
                _interactions.postValue(Event(Interactions.SUBMIT_NAME))
            }
        }
    }

    fun changeKeyboardVisibility(isOpen: Boolean) {
        viewModelScope.launch {
            _keyboardVisibilityEvent.postValue(isOpen)
        }
    }

    fun changeButtonSubmitEnabled(isEnabled: Boolean) {
        viewModelScope.launch {
            _isSubmitEnabled.postValue(isEnabled)
        }
    }

    enum class Interactions {
        CLICK_CONTINUE, SUBMIT_NAME
    }

}