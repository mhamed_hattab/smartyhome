package com.thinkit.think_it_code_challenge.ui.main

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.NavOptions
import androidx.navigation.fragment.findNavController
import androidx.transition.*
import com.thinkit.think_it_code_challenge.R
import com.thinkit.think_it_code_challenge.databinding.FragmentMainBinding
import com.thinkit.think_it_code_challenge.utils.CommonUtils
import dagger.hilt.android.AndroidEntryPoint
import io.android.remote.result.EventObserver
import kotlinx.coroutines.ExperimentalCoroutinesApi
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEvent
import net.yslibrary.android.keyboardvisibilityevent.KeyboardVisibilityEventListener

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainFragment : Fragment() {

    private val viewModel: MainViewModel by viewModels()
    private lateinit var viewBinding: FragmentMainBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentMainBinding.inflate(inflater, container, false)
        viewBinding.apply {
            viewModel = this@MainFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView()
        startObserve()
    }

    private fun initView() {
        CommonUtils.setCapitalizeTextWatcher(viewBinding.inputName)
        viewBinding.inputName.addTextChangedListener {
            viewModel.changeButtonSubmitEnabled(!it.isNullOrBlank())
        }
        KeyboardVisibilityEvent.setEventListener(
            requireActivity(),
            viewLifecycleOwner,
            object : KeyboardVisibilityEventListener {
                override fun onVisibilityChanged(isOpen: Boolean) {
                    viewModel.changeKeyboardVisibility(isOpen)
                }
            })
    }

    private fun startObserve() {
        viewModel.interactions.observe(viewLifecycleOwner, EventObserver { interaction ->
            when (interaction) {
                MainViewModel.Interactions.CLICK_CONTINUE -> {
                    viewModel.submitName(viewBinding.inputName.text.toString())
                }
                MainViewModel.Interactions.SUBMIT_NAME -> {
                    val options = NavOptions.Builder()
                        .setPopUpTo(R.id.navigation_main, true)
                        .build()
                    findNavController().navigate(R.id.navigation_home, null, options)
                }
            }
        })
        viewModel.keyboardVisibilityEvent.observe(viewLifecycleOwner, { isOpen ->
            if (isOpen) {
                viewBinding.imgHeader.animation =
                    AnimationUtils.loadAnimation(requireContext(), R.anim.translate_up)
            } else {
                viewBinding.imgHeader.animation =
                    AnimationUtils.loadAnimation(requireContext(), R.anim.translate_down)
            }
        })
    }


}