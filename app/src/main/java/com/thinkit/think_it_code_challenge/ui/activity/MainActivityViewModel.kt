package com.thinkit.think_it_code_challenge.ui.activity

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.*
import com.thinkit.think_it_code_challenge.domain.IsUserConnectedUseCase
import com.thinkit.think_it_code_challenge.domain.StatusUser
import io.android.remote.result.Event
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect

@ExperimentalCoroutinesApi
class MainActivityViewModel @ViewModelInject constructor(
    isUserConnectedUseCase: IsUserConnectedUseCase
) : ViewModel() {

    val launchDestination = liveData {
        val result = isUserConnectedUseCase.invoke(Unit)
        result.catch {
            emit(Event(Destination.MAIN))
        }.collect { resourceStatusUser ->
            when (resourceStatusUser.data) {
                StatusUser.NEW -> emit(Event(Destination.MAIN))
                StatusUser.CONNECTED -> emit(Event(Destination.HOME))
                null -> emit(Event(Destination.MAIN))
            }
        }
    }

    enum class Destination {
        HOME,
        MAIN
    }
}