package com.thinkit.think_it_code_challenge.ui.activity

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import com.thinkit.think_it_code_challenge.R
import com.thinkit.think_it_code_challenge.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint
import io.android.remote.result.EventObserver
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private lateinit var viewBinding: ActivityMainBinding
    val viewModel: MainActivityViewModel by viewModels()

    private lateinit var navController: NavController
    private var navHostFragment: NavHostFragment? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBinding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        viewBinding.apply {
            viewModel = this@MainActivity.viewModel
            lifecycleOwner = this@MainActivity
        }
        initView()
        launchDestination()
    }

    private fun initView() {
        navHostFragment = supportFragmentManager
            .findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        navController = findNavController(R.id.nav_host_fragment)
    }

    private fun launchDestination() {
        viewModel.launchDestination.observe(this, EventObserver { destination ->
            when (destination) {
                MainActivityViewModel.Destination.HOME -> {
                    startDestinationHome(true)
                }

                MainActivityViewModel.Destination.MAIN -> {
                    startDestinationHome(false)
                }
            }
        })

    }

    private fun startDestinationHome(startHome: Boolean) {
        val graph = navController.navInflater.inflate(R.navigation.main_graph)
        graph.startDestination = if (startHome) R.id.navigation_home else R.id.navigation_main

        navController.graph = graph
        navController.setGraph(graph, intent.extras)
    }


}