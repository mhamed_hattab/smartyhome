package com.thinkit.think_it_code_challenge.ui

import androidx.annotation.DrawableRes


data class SectionError(
    @DrawableRes val src: Int,
    val message: String

)
