package com.thinkit.think_it_code_challenge.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.thinkit.think_it_code_challenge.R
import com.thinkit.think_it_code_challenge.databinding.FragmentHomeBinding
import com.thinkit.think_it_code_challenge.ui.home.adapter.RoomsAdapter
import dagger.hilt.android.AndroidEntryPoint
import io.android.remote.result.EventObserver
import io.android.remote.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HomeFragment : Fragment() {

    private val viewModel: HomeViewModel by viewModels()
    private lateinit var viewBinding: FragmentHomeBinding

    private val adapterRooms by lazy {
        RoomsAdapter(viewLifecycleOwner, viewModel)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        viewModel.getUserName()
        viewModel.getDataRooms()
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        viewBinding = FragmentHomeBinding.inflate(inflater, container, false)
        viewBinding.apply {
            viewModel = this@HomeFragment.viewModel
            lifecycleOwner = viewLifecycleOwner
        }
        return viewBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initView()
        startObserve()
    }

    private fun initView() {
        viewBinding.rcvRooms.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.VERTICAL, false)
            adapter = adapterRooms
        }
    }


    private fun startObserve() {
        viewModel.userName.observe(viewLifecycleOwner, { userName ->
            viewBinding.textName.text = getString(
                R.string.user_name,
                userName
            )
        })

        viewModel.currentDate.observe(viewLifecycleOwner, { currentDate ->
            viewBinding.textDate.text = currentDate
        })

        viewModel.rooms.observe(viewLifecycleOwner, EventObserver { resource ->
            when (resource.status) {
                Status.SUCCESS -> {
                    resource.data?.let { result ->
                        if (result.isNullOrEmpty()) {
                            //todo: show empty data adapterRooms.hasEmpty
                        } else {
                            adapterRooms.rooms = result
                        }

                    }
                }
                Status.ERROR -> {
                    //todo: show error data  adapterRooms hasError
                }
                Status.LOADING -> {
                    //todo: show loading view
                }
            }
        })


    }


}