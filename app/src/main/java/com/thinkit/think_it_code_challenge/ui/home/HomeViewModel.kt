package com.thinkit.think_it_code_challenge.ui.home

import androidx.hilt.lifecycle.ViewModelInject
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.thinkit.think_it_code_challenge.domain.GetRoomsUseCase
import com.thinkit.think_it_code_challenge.domain.GetUserNameUseCase
import com.thinkit.think_it_code_challenge.utils.DateUtils
import io.android.remote.model.Room
import io.android.remote.result.Event
import io.android.remote.utils.Resource
import io.android.remote.utils.Status
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import timber.log.Timber
import java.util.*

@ExperimentalCoroutinesApi
class HomeViewModel @ViewModelInject constructor(
    private val getUserNameUseCase: GetUserNameUseCase,
    private val getRoomsUseCase: GetRoomsUseCase
) : ViewModel() {

    private val _userName = MutableLiveData<String>()
    val userName: LiveData<String> = _userName

    private val _currentDate = MutableLiveData<String>().apply {
        value = DateUtils.formatDateToString(Date()).toUpperCase(Locale.getDefault())
    }
    val currentDate: LiveData<String> = _currentDate


    private val _rooms = MutableLiveData<Event<Resource<List<Room>>>>()
    val rooms: LiveData<Event<Resource<List<Room>>>> = _rooms

    fun getUserName() {
        viewModelScope.launch {
            getUserNameUseCase.invoke(Unit).collect { resource ->
                _userName.postValue(resource.data)
            }
        }

    }

    fun getDataRooms() {
        viewModelScope.launch {
            getRoomsUseCase.invoke(Unit).collect { resource ->
                if (resource.status == Status.SUCCESS) {
                    _rooms.postValue(Event(resource))
                } else {
                    //todo
                }

            }
        }
    }


    fun onClickRefresh() {

    }

    fun onClickItemRoom(room: Room?) {
        room?.let {
            Timber.d("onClick Room")
        }
    }


}