package com.thinkit.think_it_code_challenge.ui

import androidx.annotation.DrawableRes
import androidx.annotation.StringRes

data class SectionEmptyData(
    @StringRes val titleId: Int,
    @DrawableRes val src: Int
)
