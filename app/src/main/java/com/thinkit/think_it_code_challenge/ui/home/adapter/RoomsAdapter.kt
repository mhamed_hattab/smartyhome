package com.thinkit.think_it_code_challenge.ui.home.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.LifecycleOwner
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.thinkit.think_it_code_challenge.R
import com.thinkit.think_it_code_challenge.databinding.ItemEmptyDataBinding
import com.thinkit.think_it_code_challenge.databinding.ItemErrorBinding
import com.thinkit.think_it_code_challenge.databinding.ItemRoomBinding
import com.thinkit.think_it_code_challenge.ui.home.HomeViewModel

import com.thinkit.think_it_code_challenge.ui.SectionEmptyData
import com.thinkit.think_it_code_challenge.ui.SectionError
import com.thinkit.think_it_code_challenge.utils.executeAfter

import io.android.remote.model.Room
import kotlinx.coroutines.ExperimentalCoroutinesApi
import java.util.*


@ExperimentalCoroutinesApi
class RoomsAdapter constructor(
    private val lifecycleOwner: LifecycleOwner,
    private val model: HomeViewModel
) : RecyclerView.Adapter<ViewHolderRooms>() {

    private val dataList = AsyncListDiffer<Any>(this, DiffCallback)

    var rooms: List<Room> = arrayListOf()
        set(value) {
            field = value
            dataList.submitList(buildMergedList(value))
        }
    var hasError: SectionError? = null
        set(value) {
            field = value
            dataList.submitList(buildMergedList(hasErrors = value))
        }
    var hasEmpty: SectionEmptyData? = null
        set(value) {
            field = value
            dataList.submitList(buildMergedList(isEmpty = value))
        }

    init {
        dataList.submitList(buildMergedList())
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolderRooms {
        val inflater = LayoutInflater.from(parent.context)
        return when (viewType) {

            R.layout.item_room -> ViewHolderRooms.DocumentViewHolder(
                ItemRoomBinding.inflate(inflater, parent, false)
            )
            R.layout.item_empty_data -> ViewHolderRooms.EmptyDataViewHolder(
                ItemEmptyDataBinding.inflate(inflater, parent, false)
            )
            R.layout.item_error -> ViewHolderRooms.ErrorViewHolder(
                ItemErrorBinding.inflate(inflater, parent, false)
            )
            else -> throw IllegalStateException("Unknown viewType $viewType")
        }
    }

    override fun onBindViewHolder(holder: ViewHolderRooms, position: Int) {
        when (holder) {

            is ViewHolderRooms.DocumentViewHolder -> holder.binding.executeAfter {
                val presenter = dataList.currentList[position] as Room
                room = presenter
                viewModel = this@RoomsAdapter.model
                lifecycleOwner = this@RoomsAdapter.lifecycleOwner
            }
            is ViewHolderRooms.ErrorViewHolder -> holder.binding.executeAfter {
                header = dataList.currentList[position] as SectionError
                viewModel = this@RoomsAdapter.model
            }

            is ViewHolderRooms.EmptyDataViewHolder -> holder.binding.executeAfter {
                sectionEmptyData = dataList.currentList[position] as SectionEmptyData

            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (dataList.currentList[position]) {
            is Room -> R.layout.item_room
            is SectionEmptyData -> R.layout.item_empty_data
            is SectionError -> R.layout.item_error
            else -> throw IllegalStateException("Unknown view type at position $position")
        }
    }

    override fun getItemCount() = dataList.currentList.size

    /**
     * This adapter displays heterogeneous data types but `RecyclerView` & `AsyncListDiffer` deal in
     * a single list of items. We therefore combine them into a merged list, using marker objects
     */
    private fun buildMergedList(
        roomsList: List<Room> = rooms,
        hasErrors: SectionError? = hasError,
        isEmpty: SectionEmptyData? = hasEmpty
    ): List<Any> {
        val merged = mutableListOf<Any>()
        when {
            roomsList.isNotEmpty() -> {
                merged.addAll(roomsList)
            }
            hasErrors != null -> {
                merged.clear()
                merged.add(hasErrors)
            }
            isEmpty != null -> {
                merged.clear()
                merged.add(isEmpty)
            }

        }

        return merged
    }
}


/**
 * Diff items presented by this adapter.
 */
object DiffCallback : DiffUtil.ItemCallback<Any>() {

    override fun areItemsTheSame(oldItem: Any, newItem: Any): Boolean {
        return when {
            oldItem is SectionError && newItem is SectionError -> oldItem == newItem
            oldItem is SectionEmptyData && newItem is SectionEmptyData -> oldItem == newItem
            oldItem is Room && newItem is Room -> oldItem.name == newItem.name
            else -> false
        }
    }

    @SuppressLint("DiffUtilEquals")
    override fun areContentsTheSame(oldItem: Any, newItem: Any): Boolean {
        return when {
            oldItem is Room && newItem is Room -> oldItem == newItem
            else -> true
        }
    }
}

/**
 * [RecyclerView.ViewHolder] types used by this adapter.
 */
sealed class ViewHolderRooms(itemView: View) : RecyclerView.ViewHolder(itemView) {
    class DocumentViewHolder(
        val binding: ItemRoomBinding
    ) : ViewHolderRooms(binding.root)

    class ErrorViewHolder(
        val binding: ItemErrorBinding
    ) : ViewHolderRooms(binding.root)

    class EmptyDataViewHolder(
        val binding: ItemEmptyDataBinding
    ) : ViewHolderRooms(binding.root)
}
