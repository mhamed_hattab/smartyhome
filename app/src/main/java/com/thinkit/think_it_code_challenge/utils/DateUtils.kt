package com.thinkit.think_it_code_challenge.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {


    private const val dateFormat = "MMMM dd, yyy"

    fun formatDateToString(
        date: Date,
        dateForm: String = dateFormat,
        locale: Locale = Locale.getDefault()
    ): String {
        val format = SimpleDateFormat(dateForm, locale)
        return format.format(date)
    }
}