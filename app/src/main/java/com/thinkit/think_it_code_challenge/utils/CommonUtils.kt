package com.thinkit.think_it_code_challenge.utils

import android.annotation.SuppressLint
import android.text.Editable
import android.text.TextWatcher
import com.google.android.material.textfield.TextInputEditText

object CommonUtils {
    fun setCapitalizeTextWatcher(editText: TextInputEditText) {
        val textWatcher = object : TextWatcher {

            var mStart = 0

            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

                mStart = start + count
            }

            @SuppressLint("DefaultLocale")
            override fun afterTextChanged(s: Editable) {
                val input = s.toString()
                val capitalizedText: String
                capitalizedText = if (input.isEmpty())
                    input
                else
                    input.substring(0, 1).toUpperCase() + input.substring(1)
                if (capitalizedText != editText.text!!.toString()) {
                    editText.addTextChangedListener(object : TextWatcher {
                        override fun beforeTextChanged(
                            s: CharSequence,
                            start: Int,
                            count: Int,
                            after: Int
                        ) {

                        }

                        override fun onTextChanged(
                            s: CharSequence,
                            start: Int,
                            before: Int,
                            count: Int
                        ) {

                        }

                        override fun afterTextChanged(s: Editable) {
                            editText.setSelection(mStart)
                            editText.removeTextChangedListener(this)
                        }
                    })
                    editText.setText(capitalizedText)

                }
            }
        }

        editText.addTextChangedListener(textWatcher)
    }
}
