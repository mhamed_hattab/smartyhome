package com.thinkit.think_it_code_challenge.utils

import com.thinkit.think_it_code_challenge.R
import io.android.remote.model.Room

/**
 * A sample list of rooms
 */
val SampleRooms = listOf(
    Room(R.drawable.livingroom, "Living Room", 4),
    Room(R.drawable.mediaroom, "Media Room", 6),
    Room(R.drawable.bathroom, "Bathroom", 1),
    Room(R.drawable.bedroom, "Bedroom", 3)
)