package com.thinkit.think_it_code_challenge.domain

import io.android.remote.dataManager.DataRepository
import io.android.remote.useCase.FlowUseCase
import io.android.remote.providerCoroutine.CoroutineProvider
import io.android.remote.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Set user Name in Local Data Store Use Case Flow
 */
@ExperimentalCoroutinesApi
open class SetUserNameUseCase @Inject constructor(
    private val dataManager: DataRepository,
    schedulerProvider: CoroutineProvider
) : FlowUseCase<String, Boolean>(schedulerProvider.io()) {
    override fun execute(parameters: String): Flow<Resource<Boolean>> = flow {
        dataManager.setUserConnected(true)
        dataManager.setUserName(parameters)
        emit(Resource.success(true))
    }

}
