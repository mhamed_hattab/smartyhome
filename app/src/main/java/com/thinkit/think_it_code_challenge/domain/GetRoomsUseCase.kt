package com.thinkit.think_it_code_challenge.domain

import com.thinkit.think_it_code_challenge.utils.SampleRooms
import io.android.remote.dataManager.DataRepository
import io.android.remote.useCase.FlowUseCase
import io.android.remote.model.Room
import io.android.remote.providerCoroutine.CoroutineProvider
import io.android.remote.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Get List of Rooms Use Case
 */
@ExperimentalCoroutinesApi
open class GetRoomsUseCase @Inject constructor(
    private val dataManager: DataRepository,
    schedulerProvider: CoroutineProvider
) : FlowUseCase<Unit, List<Room>>(schedulerProvider.io()) {
    override fun execute(parameters: Unit): Flow<Resource<List<Room>>> = flow {
        emit( Resource.success(SampleRooms))
    }


}
