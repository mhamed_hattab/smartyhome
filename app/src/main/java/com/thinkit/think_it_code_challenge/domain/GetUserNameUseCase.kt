package com.thinkit.think_it_code_challenge.domain

import io.android.remote.dataManager.DataRepository
import io.android.remote.useCase.FlowUseCase
import io.android.remote.providerCoroutine.CoroutineProvider
import io.android.remote.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

/**
 * Get user Name from Local Data Store Use Case
 */
@ExperimentalCoroutinesApi
open class GetUserNameUseCase @Inject constructor(
    private val dataManager: DataRepository,
    schedulerProvider: CoroutineProvider
) : FlowUseCase<Unit, String?>(schedulerProvider.io()) {
    override fun execute(parameters: Unit): Flow<Resource<String?>> = flow {
        dataManager.getUserName().catch {
            emit(Resource.success(null))
        }.collect { userName ->
            emit(Resource.success(userName))
        }
    }


}
