package com.thinkit.think_it_code_challenge.domain


import io.android.remote.dataManager.DataRepository
import io.android.remote.useCase.FlowUseCase
import io.android.remote.providerCoroutine.CoroutineProvider
import io.android.remote.utils.Resource
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flow
import javax.inject.Inject


/**
 * Check if User already typed his name and connected Use Case
 */
@ExperimentalCoroutinesApi
open class IsUserConnectedUseCase @Inject constructor(
    private val dataManager: DataRepository,
    schedulerProvider: CoroutineProvider
) : FlowUseCase<Unit, StatusUser>(schedulerProvider.io()) {
    override fun execute(parameters: Unit): Flow<Resource<StatusUser>> = flow {
        dataManager.isUserConnected().catch {
            emit(Resource.success(StatusUser.NEW))
        }.collect { isConnected ->
            if (isConnected) {
                emit(Resource.success(StatusUser.CONNECTED))
            } else {
                emit(Resource.success(StatusUser.NEW))
            }
        }
    }
}

enum class StatusUser {
    NEW, CONNECTED
}
