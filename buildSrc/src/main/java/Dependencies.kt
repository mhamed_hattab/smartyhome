package com.android.buildsrc

object App {
    const val sample = "com.thinkit.think_it_code_challenge"
}

object Modules {
    val sample = "com.thinkit.think_it_code_challenge"
    val app = ":app"
    const val remote = ":remote"
}

object Releases {
    val versionCode = 1
    val versionName = "1.0"
}

object DefaultConfig {
    const val minSdk = 21
    const val targetSdk = 30
    const val compileSdk = 30
}


object Libs {
    const val material = "com.google.android.material:material:1.3.0"
    const val androidGradlePlugin = "com.android.tools.build:gradle:4.1.1"
    const val gson = "com.google.code.gson:gson:2.8.6"
    const val timber = "com.jakewharton.timber:timber:4.7.1"

    object Kotlin {
        private const val version = "1.4.21"
        const val stdlib = "org.jetbrains.kotlin:kotlin-stdlib-jdk8:$version"
        const val gradlePlugin = "org.jetbrains.kotlin:kotlin-gradle-plugin:$version"
        const val extensions = "org.jetbrains.kotlin:kotlin-android-extensions:$version"
    }

    object Coroutines {
        private const val version = "1.4.2"
        const val core = "org.jetbrains.kotlinx:kotlinx-coroutines-core:$version"
        const val android = "org.jetbrains.kotlinx:kotlinx-coroutines-android:$version"
        const val test = "org.jetbrains.kotlinx:kotlinx-coroutines-test:$version"
    }

    object Hilt {


        private const val version = "2.28-alpha"
        private const val versionViewModel = "1.0.0-alpha01"
        const val android = "com.google.dagger:hilt-android:$version"
        const val androidCompiler = "com.google.dagger:hilt-android-compiler:$version"
        const val viewModel = "androidx.hilt:hilt-lifecycle-viewmodel:$versionViewModel"
        const val compiler = "androidx.hilt:hilt-compiler:$versionViewModel"

        const val gradlePlugin = "com.google.dagger:hilt-android-gradle-plugin:$version"
    }

    object Navigation {
        private const val version = "2.3.2"
        const val fragment = "androidx.navigation:navigation-fragment-ktx:$version"
        const val ui = "androidx.navigation:navigation-ui-ktx:$version"
        const val gradlePlugin = "androidx.navigation:navigation-safe-args-gradle-plugin:$version"
    }

    object AndroidX {
        const val appcompat = "androidx.appcompat:appcompat:1.2.0"
        const val palette = "androidx.palette:palette:1.0.0"
        const val coreKtx = "androidx.core:core-ktx:1.3.2"

        object Constraint {
            const val constraintLayout = "androidx.constraintlayout:constraintlayout:2.0.4"
        }

        object Legacy {
            const val utils = "androidx.legacy:legacy-support-core-utils:1.0.0"
            const val support = "androidx.legacy:legacy-support-v4:1.0.0"
        }
    }

    object Lifecycle {
        const val version = "2.2.0"
        const val lifecycleTesting = "2.1.0"
        const val viewmodel = "androidx.lifecycle:lifecycle-viewmodel-ktx:$version"
        const val livedata = "androidx.lifecycle:lifecycle-livedata-ktx:$version"
        const val extensions = "androidx.lifecycle:lifecycle-extensions:$version"
        const val testing = "androidx.arch.core:core-testing:$lifecycleTesting"
    }

    object Room {
        private const val version = "2.3.0-alpha01"
        const val runtime = "ndroidx.room:room-runtime:$version"
        const val compiler = "androidx.room:room-compiler:$version"
        const val ktx = "androidx.room:room-ktxr:$version"
    }

    object DataStore {
        private const val version = "1.0.0-alpha02"
        const val preferences = "androidx.datastore:datastore-preferences:$version"
    }

    object Espresso {
        private const val version = "3.1.1"
        const val contrib = "androidx.test.espresso:espresso-contrib:$version"
        const val core = "androidx.test.espresso:espresso-core:$version"
    }

    object Junit {
        private const val version = "4.13"
        const val ext = "androidx.test.ext:junit:1.1.1"
        const val junit = "junit:junit:$version"
    }
    object Rules {
        const val rules = "androidx.test:rules:1.1.1"
    }
    object Runner {
        const val runner = "androidx.test:runner:1.2.0"
    }
    object Mockito {
        const val core = "org.mockito:mockito-core:3.3.3"
        const val kotlin = "com.nhaarman:mockito-kotlin:1.5.0"
        const val hamcrest = "org.hamcrest:hamcrest-library:1.3"
        const val mockito= "com.nhaarman.mockitokotlin2:mockito-kotlin:2.2.0"

    }


}

object Deps {


}
